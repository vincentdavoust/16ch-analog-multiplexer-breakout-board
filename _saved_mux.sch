EESchema Schematic File Version 2
LIBS:mux-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mux-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "13 nov 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74HC4067 U0
U 1 1 5645A6C5
P 4650 4850
F 0 "U0" H 4400 6650 60  0000 C CNN
F 1 "74HC4067" H 4650 4850 60  0000 C CNN
F 2 "~" H 4650 4850 60  0000 C CNN
F 3 "~" H 4650 4850 60  0000 C CNN
	1    4650 4850
	1    0    0    -1  
$EndComp
Text GLabel 5150 3200 2    60   Output ~ 0
CMD0
Text GLabel 5150 3300 2    60   Output ~ 0
CMD1
Text GLabel 5150 3400 2    60   Output ~ 0
CMD2
Text GLabel 5150 3500 2    60   Output ~ 0
CMD3
Text GLabel 5150 3600 2    60   Output ~ 0
CMD4
Text GLabel 5150 3700 2    60   Output ~ 0
CMD5
Text GLabel 5150 3800 2    60   Output ~ 0
CMD6
Text GLabel 5150 3900 2    60   Output ~ 0
CMD7
Text GLabel 5150 4000 2    60   Output ~ 0
CMD8
Text GLabel 5150 4100 2    60   Output ~ 0
CMD9
Text GLabel 5150 4200 2    60   Output ~ 0
CMD10
Text GLabel 5150 4300 2    60   Output ~ 0
CMD11
Text GLabel 5150 4400 2    60   Output ~ 0
CMD12
Text GLabel 5150 4500 2    60   Output ~ 0
CMD13
Text GLabel 5150 4600 2    60   Output ~ 0
CMD14
Text GLabel 5150 4700 2    60   Output ~ 0
CMD15
Text GLabel 4100 4700 0    60   Input ~ 0
GND
Text GLabel 4100 3200 0    60   Input ~ 0
V1
$Comp
L CONN_3 K0
U 1 1 5645A7FE
P 7600 1300
F 0 "K0" V 7550 1300 50  0000 C CNN
F 1 "CONN_3" V 7650 1300 40  0000 C CNN
F 2 "~" H 7600 1300 60  0000 C CNN
F 3 "~" H 7600 1300 60  0000 C CNN
	1    7600 1300
	1    0    0    -1  
$EndComp
Text GLabel 7250 1200 0    60   Input ~ 0
GND
Text GLabel 7250 1300 0    60   Input ~ 0
VCC
Text GLabel 7250 1400 0    60   Input ~ 0
CMD0
$Comp
L CONN_3 K1
U 1 1 5645A824
P 7600 1850
F 0 "K1" V 7550 1850 50  0000 C CNN
F 1 "CONN_3" V 7650 1850 40  0000 C CNN
F 2 "~" H 7600 1850 60  0000 C CNN
F 3 "~" H 7600 1850 60  0000 C CNN
	1    7600 1850
	1    0    0    -1  
$EndComp
Text GLabel 7250 1750 0    60   Input ~ 0
GND
Text GLabel 7250 1850 0    60   Input ~ 0
VCC
Text GLabel 7250 1950 0    60   Input ~ 0
CMD1
$Comp
L CONN_3 K2
U 1 1 5645A831
P 7600 2300
F 0 "K2" V 7550 2300 50  0000 C CNN
F 1 "CONN_3" V 7650 2300 40  0000 C CNN
F 2 "~" H 7600 2300 60  0000 C CNN
F 3 "~" H 7600 2300 60  0000 C CNN
	1    7600 2300
	1    0    0    -1  
$EndComp
Text GLabel 7250 2200 0    60   Input ~ 0
GND
Text GLabel 7250 2300 0    60   Input ~ 0
VCC
Text GLabel 7250 2400 0    60   Input ~ 0
CMD2
$Comp
L CONN_3 K3
U 1 1 5645A83A
P 7600 2700
F 0 "K3" V 7550 2700 50  0000 C CNN
F 1 "CONN_3" V 7650 2700 40  0000 C CNN
F 2 "~" H 7600 2700 60  0000 C CNN
F 3 "~" H 7600 2700 60  0000 C CNN
	1    7600 2700
	1    0    0    -1  
$EndComp
Text GLabel 7250 2600 0    60   Input ~ 0
GND
Text GLabel 7250 2700 0    60   Input ~ 0
VCC
Text GLabel 7250 2800 0    60   Input ~ 0
CMD3
$Comp
L CONN_3 K4
U 1 1 5645A843
P 7600 3250
F 0 "K4" V 7550 3250 50  0000 C CNN
F 1 "CONN_3" V 7650 3250 40  0000 C CNN
F 2 "~" H 7600 3250 60  0000 C CNN
F 3 "~" H 7600 3250 60  0000 C CNN
	1    7600 3250
	1    0    0    -1  
$EndComp
Text GLabel 7250 3150 0    60   Input ~ 0
GND
Text GLabel 7250 3250 0    60   Input ~ 0
VCC
Text GLabel 7250 3350 0    60   Input ~ 0
CMD4
$Comp
L CONN_3 K5
U 1 1 5645A84C
P 7600 3700
F 0 "K5" V 7550 3700 50  0000 C CNN
F 1 "CONN_3" V 7650 3700 40  0000 C CNN
F 2 "~" H 7600 3700 60  0000 C CNN
F 3 "~" H 7600 3700 60  0000 C CNN
	1    7600 3700
	1    0    0    -1  
$EndComp
Text GLabel 7250 3600 0    60   Input ~ 0
GND
Text GLabel 7250 3700 0    60   Input ~ 0
VCC
Text GLabel 7250 3800 0    60   Input ~ 0
CMD5
$Comp
L CONN_3 K6
U 1 1 5645A855
P 7600 4100
F 0 "K6" V 7550 4100 50  0000 C CNN
F 1 "CONN_3" V 7650 4100 40  0000 C CNN
F 2 "~" H 7600 4100 60  0000 C CNN
F 3 "~" H 7600 4100 60  0000 C CNN
	1    7600 4100
	1    0    0    -1  
$EndComp
Text GLabel 7250 4000 0    60   Input ~ 0
GND
Text GLabel 7250 4100 0    60   Input ~ 0
VCC
Text GLabel 7250 4200 0    60   Input ~ 0
CMD6
$Comp
L CONN_3 K7
U 1 1 5645A85E
P 7600 4550
F 0 "K7" V 7550 4550 50  0000 C CNN
F 1 "CONN_3" V 7650 4550 40  0000 C CNN
F 2 "~" H 7600 4550 60  0000 C CNN
F 3 "~" H 7600 4550 60  0000 C CNN
	1    7600 4550
	1    0    0    -1  
$EndComp
Text GLabel 7250 4450 0    60   Input ~ 0
GND
Text GLabel 7250 4550 0    60   Input ~ 0
VCC
Text GLabel 7250 4650 0    60   Input ~ 0
CMD7
$Comp
L CONN_3 K8
U 1 1 5645A867
P 7600 4950
F 0 "K8" V 7550 4950 50  0000 C CNN
F 1 "CONN_3" V 7650 4950 40  0000 C CNN
F 2 "~" H 7600 4950 60  0000 C CNN
F 3 "~" H 7600 4950 60  0000 C CNN
	1    7600 4950
	1    0    0    -1  
$EndComp
Text GLabel 7250 4850 0    60   Input ~ 0
GND
Text GLabel 7250 4950 0    60   Input ~ 0
VCC
Text GLabel 7250 5050 0    60   Input ~ 0
CMD8
$Comp
L CONN_3 K9
U 1 1 5645A870
P 7600 5500
F 0 "K9" V 7550 5500 50  0000 C CNN
F 1 "CONN_3" V 7650 5500 40  0000 C CNN
F 2 "~" H 7600 5500 60  0000 C CNN
F 3 "~" H 7600 5500 60  0000 C CNN
	1    7600 5500
	1    0    0    -1  
$EndComp
Text GLabel 7250 5400 0    60   Input ~ 0
GND
Text GLabel 7250 5500 0    60   Input ~ 0
VCC
Text GLabel 7250 5600 0    60   Input ~ 0
CMD9
$Comp
L CONN_3 K10
U 1 1 5645A879
P 7600 5950
F 0 "K10" V 7550 5950 50  0000 C CNN
F 1 "CONN_3" V 7650 5950 40  0000 C CNN
F 2 "~" H 7600 5950 60  0000 C CNN
F 3 "~" H 7600 5950 60  0000 C CNN
	1    7600 5950
	1    0    0    -1  
$EndComp
Text GLabel 7250 5850 0    60   Input ~ 0
GND
Text GLabel 7250 5950 0    60   Input ~ 0
VCC
Text GLabel 7250 6050 0    60   Input ~ 0
CMD10
$Comp
L CONN_3 K12
U 1 1 5645A882
P 9850 4500
F 0 "K12" V 9800 4500 50  0000 C CNN
F 1 "CONN_3" V 9900 4500 40  0000 C CNN
F 2 "~" H 9850 4500 60  0000 C CNN
F 3 "~" H 9850 4500 60  0000 C CNN
	1    9850 4500
	1    0    0    -1  
$EndComp
Text GLabel 9500 4400 0    60   Input ~ 0
GND
Text GLabel 9500 4500 0    60   Input ~ 0
VCC
Text GLabel 9500 4600 0    60   Input ~ 0
CMD12
$Comp
L CONN_3 K13
U 1 1 5645A88B
P 9850 4900
F 0 "K13" V 9800 4900 50  0000 C CNN
F 1 "CONN_3" V 9900 4900 40  0000 C CNN
F 2 "~" H 9850 4900 60  0000 C CNN
F 3 "~" H 9850 4900 60  0000 C CNN
	1    9850 4900
	1    0    0    -1  
$EndComp
Text GLabel 9500 4800 0    60   Input ~ 0
GND
Text GLabel 9500 4900 0    60   Input ~ 0
VCC
Text GLabel 9500 5000 0    60   Input ~ 0
CMD13
$Comp
L CONN_3 K14
U 1 1 5645A894
P 9850 5450
F 0 "K14" V 9800 5450 50  0000 C CNN
F 1 "CONN_3" V 9900 5450 40  0000 C CNN
F 2 "~" H 9850 5450 60  0000 C CNN
F 3 "~" H 9850 5450 60  0000 C CNN
	1    9850 5450
	1    0    0    -1  
$EndComp
Text GLabel 9500 5350 0    60   Input ~ 0
GND
Text GLabel 9500 5450 0    60   Input ~ 0
VCC
Text GLabel 9500 5550 0    60   Input ~ 0
CMD14
$Comp
L CONN_3 K15
U 1 1 5645A89D
P 9850 5900
F 0 "K15" V 9800 5900 50  0000 C CNN
F 1 "CONN_3" V 9900 5900 40  0000 C CNN
F 2 "~" H 9850 5900 60  0000 C CNN
F 3 "~" H 9850 5900 60  0000 C CNN
	1    9850 5900
	1    0    0    -1  
$EndComp
Text GLabel 9500 5800 0    60   Input ~ 0
GND
Text GLabel 9500 5900 0    60   Input ~ 0
VCC
Text GLabel 9500 6000 0    60   Input ~ 0
CMD15
$Comp
L CONN_3 K11
U 1 1 5645A916
P 9850 4100
F 0 "K11" V 9800 4100 50  0000 C CNN
F 1 "CONN_3" V 9900 4100 40  0000 C CNN
F 2 "~" H 9850 4100 60  0000 C CNN
F 3 "~" H 9850 4100 60  0000 C CNN
	1    9850 4100
	1    0    0    -1  
$EndComp
Text GLabel 9500 4000 0    60   Input ~ 0
GND
Text GLabel 9500 4100 0    60   Input ~ 0
VCC
Text GLabel 9500 4200 0    60   Input ~ 0
CMD11
Text GLabel 2550 4300 2    60   Output ~ 0
GND
Text GLabel 2550 3600 2    60   Output ~ 0
V1
Wire Wire Line
	4100 4400 3500 4400
Wire Wire Line
	3500 4400 3500 4200
Wire Wire Line
	3500 4200 2550 4200
Wire Wire Line
	4100 4100 2550 4100
$Comp
L CONN_2 P0
U 1 1 5645A9B6
P 4000 1450
F 0 "P0" V 4250 1550 40  0000 C CNN
F 1 "CONN_2" H 4050 1450 40  0000 C CNN
F 2 "~" H 4000 1450 60  0000 C CNN
F 3 "~" H 4000 1450 60  0000 C CNN
	1    4000 1450
	0    -1   -1   0   
$EndComp
Text GLabel 3900 1800 0    60   Output ~ 0
GND
Text GLabel 4100 1800 2    60   Output ~ 0
VCC
$Comp
L C-RESCUE-mux C1
U 1 1 5645AA8F
P 3550 2600
F 0 "C1" H 3550 2700 40  0000 L CNN
F 1 "C" H 3556 2515 40  0000 L CNN
F 2 "~" H 3588 2450 30  0000 C CNN
F 3 "~" H 3550 2600 60  0000 C CNN
	1    3550 2600
	1    0    0    -1  
$EndComp
Text GLabel 3550 2800 0    60   Input ~ 0
GND
Text GLabel 3550 2400 0    60   Input ~ 0
V1
$Comp
L C-RESCUE-mux C0
U 1 1 5645AAB1
P 4000 2050
F 0 "C0" H 4000 2150 40  0000 L CNN
F 1 "C" H 4006 1965 40  0000 L CNN
F 2 "~" H 4038 1900 30  0000 C CNN
F 3 "~" H 4000 2050 60  0000 C CNN
	1    4000 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 1800 3900 1900
Wire Wire Line
	3900 1900 3800 1900
Wire Wire Line
	3800 1900 3800 2050
Wire Wire Line
	4100 1800 4100 1950
Wire Wire Line
	4100 1950 4200 1950
Wire Wire Line
	4200 1950 4200 2050
$Comp
L CONN_2 J0
U 1 1 5645AAEC
P 2100 1300
F 0 "J0" V 2350 1400 40  0000 C CNN
F 1 "CONN_2" H 2150 1300 40  0000 C CNN
F 2 "~" H 2100 1300 60  0000 C CNN
F 3 "~" H 2100 1300 60  0000 C CNN
	1    2100 1300
	0    -1   -1   0   
$EndComp
Text GLabel 2000 1650 3    60   Output ~ 0
V1
Text GLabel 2200 1650 3    60   Input ~ 0
VCC
Text GLabel 4100 3400 0    60   Input ~ 0
S0
Text GLabel 4100 3550 0    60   Input ~ 0
S1
Text GLabel 4100 3700 0    60   Input ~ 0
S2
Text GLabel 4100 3850 0    60   Input ~ 0
S3
Text GLabel 4100 4100 0    60   Input ~ 0
E
Text GLabel 4100 4400 0    60   Input ~ 0
Z
$Comp
L CONN_8 P1
U 1 1 5645A935
P 2200 3950
F 0 "P1" V 2150 3950 60  0000 C CNN
F 1 "CONN_8" V 2250 3950 60  0000 C CNN
F 2 "~" H 2200 3950 60  0000 C CNN
F 3 "~" H 2200 3950 60  0000 C CNN
	1    2200 3950
	-1   0    0    1   
$EndComp
Text GLabel 2550 3700 2    60   Output ~ 0
S3
Text GLabel 2550 3800 2    60   Output ~ 0
S2
Text GLabel 2550 3900 2    60   Output ~ 0
S1
Text GLabel 2550 4000 2    60   Output ~ 0
S0
$EndSCHEMATC
